# ennerd/libc

FFI based implementation of some libc functions which PHP fails to support. Currently only execv() is
available.
