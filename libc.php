<?php
namespace Ennerd\libc;

use FFI, Closure;

/**
 * Execute a command using execv from libc.
 *
 * This function provides a PHP interface to the execv function in libc. It executes a command with
 * specified arguments, completely replacing the current process image with a new process image.
 *
 * @see man 3 execv for more information about the execv function in libc.
 *
 * @param string $path The path to the binary or script to execute
 * @param array $argv An array of arguments, where the first element must be the command and the rest is arguments
 * @return int|never If the execv call is successful, the function will not return, as the current process
 *             image is completely replaced. If the execv call fails, it will return -1.
 * @throws \FFI\Exception if an error occurs when calling the execv function.
 */
function execv(string $path, array $argv = []): int {
    $ffi = FFI::cdef("
        int execv(const char *path, char *const argv[]);
    ", "libc.so.6");

    $gc = _strings($argv, $c_argv);

    $result = $ffi->execv($path, $c_argv);

    $gc();

    return $result;
}

/**
 * Utility to create a char** array of pointers to null terminated strings,
 * with a final null pointer to terminate the array.
 *
 * @param string[] $a The array
 * @param FFI\CData $result The resulting C array
 * @return Closure Function must be invoked to garbage collect the strings
 *         to avoid memory leaks.
 */
function _strings(array $arr, FFI\CData &$result=null): Closure {
    $garbage = [];
    $result = FFI::new('char *[' . 1 + count($arr) . ']');
    foreach ($arr as $k => $a) {
        if (strpos($a, "\0") !== false) {
            throw new \RuntimeException("Can't pass strings containing \\0");
        }
        $a .= "\0"; // ensure string terminates with \0
        $c_a = FFI::new('char[' . strlen($a) . ']', false);
        $garbage[] = $c_a;
        FFI::memcpy($c_a, $a, strlen($a));
        $result[$k] = $c_a;
    }
    $result[count($arr)] = null;
    return function() use ($garbage) {
        foreach ($garbage as $trash) {
            FFI::free($trash);
        }
    };
}
